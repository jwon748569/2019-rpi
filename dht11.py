# dht11.py

import Adafruit_DHT
import time
import paho.mqtt.client as mqtt

MQTT_HOST = "test.mosquitto.org"
MQTT_PORT = 1883
MQTT_KEEPALIVE_INTERVAL = 60
MQTT_TOPIC = "harang"

sensor = Adafruit_DHT.DHT11
dht_pin = 2
		
def on_publish(client, userdata, mid):
	print("Message Published..")
	client = mqtt.Client()
	client.on_publish = on_publish
	client.connect(MQTT_HOST, MQTT_PORT, MQTT_KEEPALIVE_INTERVAL)
	
	while True:
		humidity, temperature = Adafruit_DHT.read_retry(sensor, dht_pin)
		
		if humidity is not None and temperature is not None: 
			print("Temp={0}*C Humidity={1}%".format(temperature, humidity))
			client.publish(MQTT_TOPIC, value)
			print(value)
		else: 
			print("Failed to get reading.")
	
	client.loop_forever()
	client.disconnect()
	
